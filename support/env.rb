require 'rspec'
require 'faker'

require_relative '../onnisoft_api/onnisoft_api'
require_relative 'helpers/shared_context'

USERS = YAML.load_file("#{Dir.pwd}/config/users.yml")
RestClient.log = 'stdout' if ENV['DEBUG']

