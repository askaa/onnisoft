shared_examples 'check response body contains error' do |error|
  describe 'check response body' do
    it "includes #{error} error" do
      expect(@response.body).to include(error), "No error in response. Response body is #{@response.body}."
    end
  end
end


shared_examples 'check response code starts with' do |code|
  describe 'check response code' do
    it "is #{code}" do
      expect(@response.code.to_s).to include(code.to_s), "Expect response code starts with #{code}, but actual is #{@response.code}."
    end
  end
end

shared_examples 'response includes token' do
  describe 'account response' do
    it 'includes token' do
      expect(parse_json(@response.body)['token']).not_to be_nil, "No token in response. Response body is #{@response.body}."
    end
  end
end

shared_examples 'response body includes balance' do
  describe 'response body' do
    it 'includes balance' do
      expect(parse_json(@response.body)).not_to be_nil, "No body in response. Response body is #{@response.body}."
      expect(parse_json(@response.body)['balance']).not_to be_nil, "No balance in response. Response body is #{@response.body}."
    end
  end
end

shared_context 'Log in as simple user' do
  before(:all) do
    valid_user = USERS['valid_user']
    @response = OnnisoftApi.account.login(body: valid_user)
  end
end

def parse_json(response)
  JSON.parse(response)
rescue JSON::ParserError
  raise JSON::ParserError, "Response is not in JSON format. Response is: #{response}"
end