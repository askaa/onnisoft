module OnnisoftApi

  class AccountApi

    include OnnisoftApi::Helpers

    def register(**params)
      post(URL + 'account/register', params[:body].to_json)
    end

    def login(**params)
      body = params[:body] ? params[:body] : {userName: params[:username], password: params[:password]}
      post(URL + 'account/login', body.to_json)
    end


  end

end
