module OnnisoftApi

  class WalletApi

    include OnnisoftApi::Helpers

    def initialize(username='test.it')
      @username = username
      @token = get_auth_token
    end

    def deposit(username = @username, **params)
      token = params[:token] ? params[:token] : @token
      post(URL + "wallet/#{username}/deposit", params[:body].to_json, token = token)
    end

    def withdraw(username = @username, **params)
      token = params[:token] ? params[:token] : @token
      post(URL + "wallet/#{username}/withdraw", params[:body].to_json, token = token)
    end

    def balance(username = @username, **params)
      token = params[:token] ? params[:token] : @token
      get(URL + "wallet/#{username}/balance", token = token)
    end

  end

end