require 'rest-client'

require_relative 'helpers/helpers'
require_relative 'account'
require_relative 'wallet'


module OnnisoftApi

  URL = 'https://qa-test-develop.marlin.onnisoft.com/api/'
  class << self

    def account; AccountApi.new; end
    def wallet; WalletApi.new; end

  end
end
