module RestClient
  class Response < String
    def body
      String.new(self)
    rescue
      http_body
    end
  end
end


module OnnisoftApi

  module Helpers

    def get_auth_token(username='test.it', password ='Test123$')
      response =  OnnisoftApi.account.login( username: username, password: password)
      raise 'Response is not 200' unless response.code == 200
      raise 'Token is missing' unless parsed_json(response)['token']
      puts response.body
      parsed_json(response)['token']
    end

    def get(url, token = nil)
      header = {accept: 'application/json'}
      header.merge! token_headers(token) if token
      puts header
      RestClient.get url, header
    rescue RestClient::BadRequest, RestClient::InternalServerError => e
      e.response
    end

    def post(url, body, token = nil)
      header = {'Content-Type'=>'application/json-patch+json', accept: 'application/json'}
      header.merge! token_headers(token) if token
      RestClient.post(url, body, header)
    rescue RestClient::BadRequest, RestClient::InternalServerError, RestClient::Unauthorized => e
      e.response
    end

    def is_json?(body)
      begin
        return JSON.parse(body) != nil && @raw_response['Content-Type'].include?('application/json')
      rescue
        false
      end
    end

    def parsed_json(response)
      JSON.parse(response.force_encoding('UTF-8'))
    end

    private
    def token_headers(token)
      {Authorization: "Bearer #{token}"}
    end

  end

end