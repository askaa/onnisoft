## To run tests:

1. Install ruby [link](https://www.ruby-lang.org/en/downloads/)
2. If you have old ruby version you can update it using [RVM](https://rvm.io/rvm/install)

In terminal: 

3. **gem install bundler**
4. **bundle install**
4. **rspec** 

If you want to see the requests you should run tests with **DEBUG=true rspec**
