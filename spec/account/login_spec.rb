describe 'Check login functionality' do
  describe 'Positive cases' do
    describe 'login with valid user' do
      before(:all) do
        valid_user = USERS['valid_user']
        @response = OnnisoftApi.account.login(body: valid_user)
      end
      include_examples 'check response code starts with', 200
      include_examples 'response includes token'
    end
  end

  describe 'Negative cases' do
    describe('User with invalid login') do
      before(:all) do
        user = USERS['invalid_username']
        @response = OnnisoftApi.account.login(body: user)
      end
      include_examples 'check response code starts with', 400
      include_examples 'check response body contains error', 'Invalid user'
    end

    describe('User with invalid password') do
      before(:all) do
        user = USERS['invalid_password']
        @response = OnnisoftApi.account.login(body: user)
      end
      include_examples 'check response code starts with', 400
      include_examples 'check response body contains error', 'Invalid user'
    end

    describe('Invalid empty body') do
      before(:all) do
        body = {}
        @response = OnnisoftApi.account.login(body: body)
      end
      include_examples 'check response code starts with', 4
      include_examples 'check response body contains error', ''
    end

  end
end