describe 'Check registration functionality' do

  describe 'Positive cases' do
    before(:all) do
      new_valid_user = get_new_user(password: 'Qwerty$123')
      @response = OnnisoftApi.account.register(body: new_valid_user)
    end
    include_examples 'check response code starts with', 200
    include_examples 'response includes token'
  end

  describe 'Negative cases' do
    describe 'Password' do
      describe('User with only Alphanumeric password') do
        4.times do
          password = Faker::Internet.password(10, 20, true, false)
          before(:all) do
            invalid_user = get_new_user(password: password)
            @response = OnnisoftApi.account.register(body: invalid_user)
          end
          context("User with password: #{password}") do
            include_examples 'check response body contains error', 'PasswordRequiresNonAlphanumeric'
            include_examples 'check response code starts with', 400
          end
        end
      end

      describe('User without digit password') do
        %w(Qwerwrw hycddf&*# oLoloLo$"').each do |password|
          before(:all) do
            invalid_user = get_new_user(password: password)
            @response = OnnisoftApi.account.register(body: invalid_user)
          end
          context("User with password: #{password}") do
            include_examples 'check response body contains error', 'PasswordRequiresDigit'
            include_examples 'check response code starts with', 400
          end
        end

      end

      describe('User without uppercase password') do
        4.times do
          password = Faker::Internet.password(10, 20, false, true)
          before(:all) do
            invalid_user = get_new_user(password: password)
            @response = OnnisoftApi.account.register(body: invalid_user)
          end
          context("User with password: #{password}") do
            include_examples 'check response body contains error', 'PasswordRequiresUpper'
            include_examples 'check response code starts with', 400
          end
        end
      end
    end

    describe('User with invalid email') do
      %w(test@testtest testtest.test).each do |email|
        before(:all) do
          invalid_user = get_new_user(email: email)
          @response = OnnisoftApi.account.register(body: invalid_user)
        end
        context("User with email: #{email}") do
          include_examples 'check response code starts with', 400
          include_examples 'check response body contains error', 'Email'
        end
      end
    end

    describe('Already registered username') do
      before(:all) do
        invalid_user = get_new_user(username: USERS['valid_user']['userName'])
        @response = OnnisoftApi.account.register(body: invalid_user)
      end
      include_examples 'check response code starts with', 400
      include_examples 'check response body contains error', 'DuplicateUserName'
    end

    describe('Invalid username') do
      before(:all) do
        invalid_user = get_new_user(username: '')
        @response = OnnisoftApi.account.register(body: invalid_user)
      end
      include_examples 'check response code starts with', 400
      include_examples 'check response body contains error', 'InvalidUserName'
    end

    describe('Already registered email') do
      before(:all) do
        invalid_user = get_new_user(email: USERS['valid_user']['email'])
        @response = OnnisoftApi.account.register(body: invalid_user)
      end
      include_examples 'check response code starts with', 400
      include_examples 'check response body contains error', 'DuplicateEmail'
    end

    describe('Invalid body fields') do
      before(:all) do
        invalid_fields = { email: Faker::Internet.email, password: 'Qwerty$123', user_namee: Faker::Internet.username}
        @response = OnnisoftApi.account.register(body: invalid_fields)
      end
      include_examples 'check response code starts with', 400
      include_examples 'check response body contains error', ''

    end

    describe('Invalid empty body') do
      before(:all) do
        @response = OnnisoftApi.account.register(body: {})
      end
      include_examples 'check response code starts with', 4
      include_examples 'check response body contains error', ''
    end

  end
end

def get_new_user(**params)
  {
      email: params[:email] ? params[:email] : Faker::Internet.email,
      password: params[:password] ? params[:password] : Faker::Internet.password(20, 30, true, true),
      userName: params[:username] ? params[:username] : Time.now.to_i.to_s
  }
end



