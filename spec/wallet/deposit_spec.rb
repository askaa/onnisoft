describe 'Check deposit functionality' do
  describe 'Positive cases' do
    describe 'User can take deposit' do
      before(:all) do
        @response = OnnisoftApi.wallet.deposit(body: {'amount': 5})
      end
      include_examples 'check response code starts with', 200
      include_examples 'response body includes balance'

      describe 'Check balance has changed'

    end
  end

  describe 'Negative cases' do

    describe 'Deposit too big amount'
    describe 'Deposit invalid amount'
    describe 'Deposit for invalid user'

  end

end