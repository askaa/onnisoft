describe 'Check withdraw functionality' do

  describe 'Positive cases' do
    describe 'User can take deposit' do
      before(:all) do
        @response = OnnisoftApi.wallet.withdraw(body: {'amount': rand(1..100)})
      end
      include_examples 'check response code starts with', 200
      include_examples 'response body includes balance'

      describe 'Check balance has changed'

    end
  end

  describe 'Negative cases' do
    describe 'Invalid user' do
      describe 'Check error while input incorrect user in url' do
        before(:all) do
          @response = OnnisoftApi.wallet.withdraw('test', body: {'amount': rand(1..100)})
        end
        include_examples 'check response code starts with', 4
        include_examples 'check response body contains error', ''
      end

      describe 'Check error when send request with invalid token' do
        before(:all) do
          @response = OnnisoftApi.wallet.withdraw('test', body: {'amount': rand(1..100)}, token: Faker::Internet.password(245))
        end
        include_examples 'check response code starts with', 4
        include_examples 'check response body contains error', 'Unauthorized'
      end

      describe 'Check error when send request with empty token' do
        before(:all) do
          @response = OnnisoftApi.wallet.withdraw('test', body: {'amount': rand(1..100)}, token: '')
        end
        include_examples 'check response code starts with', 4
        include_examples 'check response body contains error', 'Unauthorized'
      end
    end

    describe 'Check error when send invalid amount' do
      [-1, 'ten', 00].each do |amount|
        before(:all) do
          @response = OnnisoftApi.wallet.withdraw('test', body: {'amount': amount}, token: '')
        end
        include_examples 'check response code starts with', 4
        include_examples 'check response body contains error', 'Invalid'
      end

    end
  end

end