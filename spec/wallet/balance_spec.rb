describe 'Check balance functionality' do

  describe 'Positive cases' do
    describe 'User can check balance' do
      before(:all) do
        @response = OnnisoftApi.wallet.balance
      end
      include_examples 'check response code starts with', 200
      include_examples 'response body includes balance'

    end
  end

  describe 'Negative cases' do
    describe 'Balance for invalid user'
  end

end